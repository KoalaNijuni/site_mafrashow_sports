//Treino de basquete do treinador Mafrashow
//O código recebe 3 variáveis Q, T e X e dá apenas 1 saída (A quantidade de jogadores livres durante o treino)

function Executar() {
  var T = document.getElementById("InputT").value;
  var Q = document.getElementById("InputQ").value;
  var X = document.getElementById("InputX").value;

  var rodadas = [];
  for (var i = 0; i < T + X - 1; i++) {
    rodadas.push(0);
  }

  var total = 0;

  for (var i = 0; i < T; i++) {
    rodadas[i] += Q - 1;

    for (var j = 1; j < X; j++) {
      rodadas[i + j] += -1;
    }
    total += rodadas[i];
  }

  document.getElementById("resposta").innerText = total.toString();
}
